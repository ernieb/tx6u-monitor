TX6U Monitor is a simple application for receiving data from a La Crosse TX6U temperature
sensor. The project implements a user space ISR for servicing incomming data and a main
application that handles collection and analysis of the ISR data.

![Project Picture](http://biancarelli.org/images/TX6U_receiver.jpg)

__Platform__ Raspberry Pi

__Required Libraries__ [WiringPi][1]

__Hardware Used__ 

* Raspberry Pi B+
* [433Mhz RF Recevier][2]
* breadboard and jumpers

## Reference Links
[received wave forms](http://www.f6fbb.org/domo/sensors/tx_signals.php)

[message format](http://www.f6fbb.org/domo/sensors/tx3_th.php)

[433Mhz receiver](https://www.sparkfun.com/products/10532)

[GPIO library](https://projects.drogon.net/raspberry-pi/wiringpi)


## Building and Running
`$> make`

`$> sudo ./tx6uMonitor 2`

---
Copyright (c) 2014, Ernest Biancarelli
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.

[1]: http://wiringpi.com
[2]: https://www.sparkfun.com/products/10532