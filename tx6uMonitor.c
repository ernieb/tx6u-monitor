/**
    Monitoring application for receiving data from the TX6U La Crosse sensor

    This application uses a system semaphore to coordination between the 
    ISR, in tx6uISR.c, and the data processing logic in this file.

    References:
        https://projects.drogon.net/raspberry-pi/wiringpi/ -- GPIO library
**/
/*
Copyright (c) 2014, Ernest Biancarelli
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

#include<semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <wiringPi.h>

/*
#define TRUE 1
#define FALSE 0
*/
#define BYTETOBINARYPATTERN "%d%d%d%d%d%d%d%d"
#define BYTETOBINARY(byte)  \
  (byte & 0x80 ? 1 : 0), \
  (byte & 0x40 ? 1 : 0), \
  (byte & 0x20 ? 1 : 0), \
  (byte & 0x10 ? 1 : 0), \
  (byte & 0x08 ? 1 : 0), \
  (byte & 0x04 ? 1 : 0), \
  (byte & 0x02 ? 1 : 0), \
  (byte & 0x01 ? 1 : 0)

#define PREAMBLE    0x0A
#define SENSOR_TYPE 0xF0000000
#define SENSOR_ID   0x0FE00000
#define PARITY_BIT  0x00100000
#define MEASUREMENT 0x000FFF00
#define IGNORE      0x000000FF

sem_t signal;

unsigned int data;

int lookingForMsgStart = TRUE;
int processingPayload = TRUE;
int payloadIndex = 31;
int crcIndex = 3;

unsigned char shiftReg = 0;
int payload, crc;

extern void handleGpioInterrupt(void);
void waitForData();
int processData();
float getReceivedValue();


void waitForData()
{
    sem_wait(&signal);
}

int main(int argc, char *argv[]) 
{
    int gpio_pin = 0;
    
    // Parse arguments
    if (argc < 2)
    {
        printf("Usage: ./tx6uMonitor 'gpio pin'\n");
        return -1;
    }

    gpio_pin = atoi(argv[1]);

    // Set up GPIO access library
    if(wiringPiSetup() == -1)
    {
        printf("Could not initialize GPIO\n");
        return -1;
    }

    // Set up semaphore. This is used to signal data from the ISR
    int result  = sem_init(&signal, 0,0);
    if(result != 0)
    {
        printf("Cant init semaphore");
        return -1;
    }

    // Connect ISR
    wiringPiISR(gpio_pin, INT_EDGE_BOTH, &handleGpioInterrupt);

    float temperature = 0.0;
    int status = 0;

    while(TRUE)
    {
        temperature = 0.0;
        waitForData();

        // Handle data from ISR
        status  = processData();
        if (status == TRUE)
        {
            temperature = getReceivedValue();
            printf("Temperature: %f\n\n", temperature);
        }
    } // while (forever)
    return 0;
}

float getReceivedValue()
{
    int sensorType = 0;
    int sensorId = 0;
    int measurement = 0;
    float measurement_f = 0.0;
    int calcCRC = 0;

    // As described in tx6uISR.c
    // bit    | description
    // 0-3    | sensor type
    // 4-10   | sensor id
    // 11     | parity bit (?)
    // 12-23  | measurement
    // 24-31  | ignore

    sensorType = (payload & SENSOR_TYPE) >> 28;
    sensorId = (payload & SENSOR_ID) >> 21;
    measurement = payload & MEASUREMENT;
    
    int tens = measurement >> 16;
    tens = tens * 10;
    int ones = (measurement & 0x0F000) >> 12;
    float decimal = (measurement & 0x00F00) >> 8;
    decimal = decimal / 10.0;

    measurement_f = (tens + ones + decimal);

    printf("  Payload ["BYTETOBINARYPATTERN" "BYTETOBINARYPATTERN" "BYTETOBINARYPATTERN" "BYTETOBINARYPATTERN"]\n",
    BYTETOBINARY(payload>>24), BYTETOBINARY(payload>>16), BYTETOBINARY(payload>>8), BYTETOBINARY(payload));

    //printf("CRC ["BYTETOBINARYPATTERN"]\n", BYTETOBINARY(s_crc));

    printf ("Sensor Type: %d\t Sensor Id: %d\t Measurement: %f\n", sensorType, sensorId, measurement_f);

    return measurement_f;
}

int processData() 
{
    int val = data;
    // Processing PREAMBLE?
    if (lookingForMsgStart == TRUE)
    {
        shiftReg = shiftReg << 1;
        if (val == 1) shiftReg = shiftReg | 0x01;

        if (shiftReg == PREAMBLE)
        {
            time_t rawtime;
            struct tm * timeinfo;
            char buffer [80];

            rawtime = time(NULL);
            timeinfo = localtime (&rawtime);

            strftime (buffer,80,"%I:%M:%S",timeinfo);
            printf("%s PREAMBLE ["BYTETOBINARYPATTERN"]", buffer, BYTETOBINARY(shiftReg));
            lookingForMsgStart = FALSE;
            processingPayload = TRUE;

            // Reset data payload variables
            payload = 0;
            crc = 0;
        }
#ifdef DEBUG
        else
        {
            printf("ShiftReg: "BYTETOBINARYPATTERN, BYTETOBINARY(shiftReg));
            printf("\n");
        }
#endif
        // always return when in the preamble search
        return FALSE;
    }

    // PREAMBLE was found. Read in the next 36 values to create the message
    // payload is 32 bits
    if (processingPayload == TRUE)
    {    
        if (val == 1) payload = payload | (1 << payloadIndex);

        if (payloadIndex == 0)
        {
            processingPayload = FALSE;
            payloadIndex = 32;
        }
        payloadIndex--;
    }
    else
    {
        // crc is 4 bits
        if (val == 1) crc = crc | (1 << crcIndex);

        if (crcIndex == 0)
        {
            // restart the search for new messages
            lookingForMsgStart = TRUE;
            crcIndex = 4;
        }
        crcIndex--;
    }

    if (lookingForMsgStart == TRUE)
    {
        // data collected. need to have it turned into real data
        return TRUE;
    }
} // processData()

