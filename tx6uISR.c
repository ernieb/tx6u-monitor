/**
    Raspberry Pi ISR for TX6U LaCrosse Temperature Sensor
   
    Reference Links:
        http://www.f6fbb.org/domo/sensors/tx_signals.php  -- wave forms
        http://www.f6fbb.org/domo/sensors/tx3_th.php -- message format
        https://www.sparkfun.com/products/10532 -- 433Mhz receiver
        https://projects.drogon.net/raspberry-pi/wiringpi/ -- GPIO library

    The TX6U La Crosse temperature sensors work by sending data, 1 time
    per minute (approximately) on the 433Mhz channel. 

    Bits arrive at the receiver according to the waveforms at the first link
    above. Replicated here in case the link is dead.

    Bit 0 -- Total frame size is ~2.3ms, data is ~1.3ms, break word is ~1ms

      +----------------+
      |                |    
   ___|                |_______________|
       <---  1.3ms ---><----  1ms  --->

    Bit 1 -- Total frame size is ~1.6ms, data is ~0.5ms, break word is ~1.1ms

      +-------+
      |       |    
   ___|       |_______________|
       <0.5ms ><---- 1.1ms --->

    Once receiving bits, the complete message from the sensor is described in the 
    second link. Replicated here in case the link is dead.

    Each message is 44 bits long. It is composed of:

    2 nibbles (start sequence 0x0A)
    1 nibble (sensor type. 0 = temp)
    7 bits (sensor id)
    1 bit (parity)
    8 nibbles (data)
    1 nibble (checksum)

                           Parity Bit
      Preamble  ST     Id    | 10s   1s  .0   10s  1s   CRC
      +-------+ +--+ +------+| +--+ +--+ +--+ +--+ +--+ +--+
      0000 1010 0000 0000 1110 0111 0011 0001 0111 0011 1101
        0    A    0    0   7 0   7    3    1    7    3    D    
            Checksum: (0 + A + 0 + 0 + E + 7 + 3 + 1 + 7 + 3) and F = D     

    The measure is on 3 BCD digits, the two first digits are repeated to fill the 5 nibles.

    Acknowlegement:
        https://github.com/pwdng/WeatherMonitor 
            provided the initial code. I rewrote the various functions to my style.
        https://projects.drogon.net/raspberry-pi/wiringpi/
            provided easy access to the GPIO on the Pi

**/
/*
Copyright (c) 2014, Ernest Biancarelli
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

#include<semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <wiringPi.h>
#include <sys/time.h>

#define BYTETOBINARYPATTERN "%d%d%d%d%d%d%d%d"
#define BYTETOBINARY(byte)  \
  (byte & 0x80 ? 1 : 0), \
  (byte & 0x40 ? 1 : 0), \
  (byte & 0x20 ? 1 : 0), \
  (byte & 0x10 ? 1 : 0), \
  (byte & 0x08 ? 1 : 0), \
  (byte & 0x04 ? 1 : 0), \
  (byte & 0x02 ? 1 : 0), \
  (byte & 0x01 ? 1 : 0) 

/*#define TRUE 1
#define FALSE 0
*/

struct timeval currentTime, lastTime;
int dataWordTime, breakWordTime;

int initialAcquisition = TRUE;
int bitOne = FALSE, bitZero = FALSE;

// Globals
extern unsigned int data;
extern sem_t signal;

void handleGpioInterrupt()
{
    int gpio_pin = 2; // TODO: This should be passed in
    int val = digitalRead(gpio_pin);

    gettimeofday (&currentTime, NULL) ;

    if (val == 1) // signal is high, rising edge trigger
    {
        // Ignore other interrupts until the first rising edge is seen
        if (initialAcquisition)
        {
            lastTime = currentTime;
            initialAcquisition = FALSE;
#ifdef DEBUG
            printf("InitialAcquitision complete\n");
#endif
            return;
        }

        // get time delta between last measurement and now
        if (currentTime.tv_usec < lastTime.tv_usec)    // Counter wrapped
        {
            breakWordTime = (1000000 + currentTime.tv_usec) - lastTime.tv_usec;
        }
        else
        {
            breakWordTime = currentTime.tv_usec - lastTime.tv_usec ;
        }
        lastTime = currentTime;

        // look for a timeframe +/- from 1ms. There is a wide range here
        // to account for bad signals and latency to service the interrupt
        if ((breakWordTime > 850) && (breakWordTime < 1300))
        { 
            // valid low (or break) signal
            if (bitOne == TRUE)
            {
                sem_post(&signal);
                data = 1;
#ifdef DEBUG
                printf("[1] Detected\tData Word:%4d \tBreakWord: %4d\tFrame Size: %4d\n",
                     dataWordTime, breakWordTime, dataWordTime+breakWordTime);
#endif
            }
            if (bitZero == TRUE)
            {
                sem_post(&signal);
                data = 0;
#ifdef DEBUG
                printf("[0] Detected\tData Word:%4d \tBreakWord: %4d\tFrame Size: %4d\n",
                     dataWordTime, breakWordTime, dataWordTime+breakWordTime);
#endif
            }
        }
        bitOne = FALSE;
        bitZero = FALSE;
    }
    else // signal is low, falling edge trigger
    {
        if (initialAcquisition == TRUE) return;

        // get time delta between last measurement and now
        if (currentTime.tv_usec < lastTime.tv_usec)    // Counter wrapped
        {
            dataWordTime = (1000000 + currentTime.tv_usec) - lastTime.tv_usec;
        }
        else
        {
            dataWordTime = currentTime.tv_usec - lastTime.tv_usec ;
        }

        lastTime = currentTime;
 
        // look for a timeframe +/- from 0.5ms. There is a wide range here
        // to account for bad signals and latency to service the interrupt
        if ((dataWordTime > 350) && (dataWordTime < 900))
        { 
            // bit 1 start caught
            bitOne = TRUE;
            bitZero = FALSE;
        }

        // look for a timeframe +/- from 1.3ms. There is a wide range here
        // to account for bad signals and latency to service the interrupt
        if ((dataWordTime > 800) && (dataWordTime < 1600))
        {
            // bit 0 start caught
            bitZero = TRUE;
            bitOne = FALSE;
        }

        // handle garbage/errors receiving
        if ((dataWordTime < 350) || (dataWordTime > 1600))
        {
            bitZero = bitOne = FALSE;
        }              
    }
}

